package com.example.springdataes.config;

import com.sun.xml.internal.ws.wsdl.writer.document.Port;
import lombok.Data;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

@ConfigurationProperties(prefix = "elasticsearch")
@Data
@Configuration
public class SpringDataElasticSearchConfig extends AbstractElasticsearchConfiguration {

    private String hostname;
    private int port;

    @Override
    public RestHighLevelClient elasticsearchClient() {
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(RestClient.builder(new HttpHost(hostname,port,"http")));
        return restHighLevelClient;
    }
}
