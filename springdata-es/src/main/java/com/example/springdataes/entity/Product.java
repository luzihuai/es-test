package com.example.springdataes.entity;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.elasticsearch.annotations.*;

@Data
@ToString
@Document(indexName = "product", createIndex = true)
public class Product {

    @Id
    private Long id;

    @Field(name = "productname",type = FieldType.Text)
    private String productname;

    @Field(name = "price",type = FieldType.Double)
    private double price;

    @Field(name = "photo",type = FieldType.Text)
    private String photo;
}
