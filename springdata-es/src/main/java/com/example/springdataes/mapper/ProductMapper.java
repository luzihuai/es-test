package com.example.springdataes.mapper;

import com.example.springdataes.entity.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductMapper extends  ElasticsearchRepository<Product, Long> {
}
