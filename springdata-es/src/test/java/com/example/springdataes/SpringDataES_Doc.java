package com.example.springdataes;


import com.example.springdataes.entity.Product;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.BulkOptions;
import org.springframework.data.elasticsearch.core.query.ByQueryResponse;
import org.springframework.data.elasticsearch.core.query.StringQuery;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.Serializable;
import java.util.ArrayList;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class SpringDataES_Doc {


    @Autowired
    ElasticsearchRestTemplate elasticsearchRestTemplate;

    /**
     * 文档保存
     */
    @Test
    public void testCreateDoc(){
        Product product = new Product();
        product.setId(1L);
        product.setProductname("小熊饼干");
        product.setPrice(12.00);
        product.setPhoto("www.baidu.com");
        Product save = elasticsearchRestTemplate.save(product);
        System.out.println("保存成功");
    }

    /**
     * 修改文档
     */
    @Test
    public void testUpdateDoc(){
        Product product = new Product();
        product.setId(1L);
        product.setProductname("小熊饼干001");
        product.setPrice(11.00);
        product.setPhoto("www.baidu.com");
        Product save = elasticsearchRestTemplate.save(product);
        System.out.println("修改成功");
    }

    /**
     * 批量新增
     */
    @Test
    public void testCreateDocBatch(){
        ArrayList<Product> objects = new ArrayList<>();
        for (int i = 0 ; i<10 ; i++){
            Product product = new Product();
            product.setId(Long.valueOf(i+2));
            product.setProductname("小熊饼干" + i+2);
            product.setPrice(Double.valueOf(i+14));
            product.setPhoto("www.baidu.com"+i);
            objects.add(product);
        }
        IndexCoordinates product1 = IndexCoordinates.of("product");
        Iterable<Product> save = elasticsearchRestTemplate.save(objects, product1);
        System.out.println("批量新增");
    }

    /**
     * 删除文档
     */
    @Test
    public void testDeleteDoc(){
        /*
        *   此删除方法中ID 为 String 类型
        */
        String delete = elasticsearchRestTemplate.delete("1", Product.class);

    }

    /**
     * 批量删除文档
     */
    @Test
    public void testDeleteDocBicth(){
        StringQuery age = new StringQuery(QueryBuilders.matchQuery("price", 14.0).toString());
        ByQueryResponse product = elasticsearchRestTemplate.delete(age, Product.class, IndexCoordinates.of("product"));
        System.out.println(product);
    }
}
