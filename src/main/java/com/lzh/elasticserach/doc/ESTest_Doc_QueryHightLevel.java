package com.lzh.elasticserach.doc;

import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.MaxAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;

public class ESTest_Doc_QueryHightLevel {


    public static void main(String[] args) throws IOException {
        //创建es client
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"))
        );

        SearchRequest request = new SearchRequest();
        //条件查询 termQuery
//        request.indices("user").source(new SearchSourceBuilder().query(QueryBuilders.termQuery("age",30)));
//        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //分页查询
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(QueryBuilders.matchAllQuery());
        //(当前页码 - 1) * 每页的数据条数
//        builder.from(2);
//        builder.size(2);
//        request.indices("user");
//        request.source(builder);


        //查询排序
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(QueryBuilders.matchAllQuery());
//        builder.sort("age", SortOrder.DESC);
//        request.indices("user");
//        request.source(builder);

        //过滤字段
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(QueryBuilders.matchAllQuery());
//        String[] include = {"name"}; //包含
//        String[] exclude = {"age"}; //排除
//        builder.fetchSource(include,exclude);
//        request.indices("user");
//        request.source(builder);

        //组合查询
//        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
//        boolQueryBuilder.should(QueryBuilders.matchQuery("age",30));
////        boolQueryBuilder.must(QueryBuilders.matchQuery("name","张三"));
//        boolQueryBuilder.should(QueryBuilders.matchQuery("age",18));
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(boolQueryBuilder);
//        request.indices("user");
//        request.source(builder);


        //范围查询
//        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("age");
//        rangeQueryBuilder.gte(30);
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(rangeQueryBuilder);
//        request.indices("user");
//        request.source(builder);

        //模糊查询
//        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("name", "张").fuzziness(Fuzziness.ONE);
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(fuzzyQueryBuilder);
//        request.indices("user");
//        request.source(builder);


        //高亮查询
//        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("age", "18");
//        SearchSourceBuilder builder = new SearchSourceBuilder().query(termQueryBuilder);
//        builder.highlighter(new HighlightBuilder()
//                .preTags("<font color='red'>")
//                .postTags("</font>")
//                .field("name"));
//
//        request.indices("user");
//        request.source(builder);


        //聚合查询
//        SearchSourceBuilder builder = new SearchSourceBuilder();
//        MaxAggregationBuilder aggregationBuilder = AggregationBuilders.max("maxAge").field("age");
//        builder.aggregation(aggregationBuilder);
//        request.indices("user");
//        request.source(builder);

        //分组查询
        SearchSourceBuilder builder = new SearchSourceBuilder();
        TermsAggregationBuilder aggregationBuilder = AggregationBuilders.terms("groupAge").field("age");
        builder.aggregation(aggregationBuilder);
        request.indices("user");
        request.source(builder);
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);
        for (SearchHit hit:
             response.getHits()) {
            System.out.println(hit.getSourceAsString());
        }
        //关闭es client
        esClient.close();
    }
}
