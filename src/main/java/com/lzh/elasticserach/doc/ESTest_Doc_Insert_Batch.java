package com.lzh.elasticserach.doc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lzh.elasticserach.entity.User;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;

public class ESTest_Doc_Insert_Batch {

    public static void main(String[] args) throws IOException {
        //创建es client
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"))
        );
        BulkRequest request = new BulkRequest();
        request.add(new IndexRequest().index("user").id("10001").source(XContentType.JSON,"name","张三","age",30));
        request.add(new IndexRequest().index("user").id("10002").source(XContentType.JSON,"name","李四","age",18));
        request.add(new IndexRequest().index("user").id("10003").source(XContentType.JSON,"name","王五","age",15));

        BulkResponse response = esClient.bulk(request, RequestOptions.DEFAULT);
        System.out.println(response);
        //关闭es client
        esClient.close();
    }
}
